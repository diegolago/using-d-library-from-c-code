#include <stdio.h>

extern int get_number();

int
main(void) {
	int number = get_number();
	printf("The number is: %d\n", number);
}

