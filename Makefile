
all: libdlib.so main

libdlib.so: lib.d
	dmd -shared -fPIC -defaultlib=libphobos2.so -of$@ $<

main: main.c
	gcc -L. $< -o $@ -ldlib

.PHONY: clean run

clean:
	rm -f libdlib.so *.o main

run: all
	LD_LIBRARY_PATH=. ./main

