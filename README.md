# Proof of concept on how to link a C program against a D library

## Description

This a proof of concept on how to use a D library from a C program.
Main files are:

* `lib.d`: D library that exports a function to be used by the C program.
* `main.c`: C program that is linked against the D library and use its function.

## Build

```bash
make
```

This generates a `libdlib.so` with the exported D function, and a `main` binary
that is linked against `libdlib.so` and uses the exported function.

Use `make clean` to clean temporaries.

## Test

```bash
make run
```

This runs `make` and then runs the `main` binary with the correct `LD_LIBRARY_PATH`.

